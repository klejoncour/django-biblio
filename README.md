# Projet Biblio Django
By Kévin LE JONCOUR - Quentin BIHET

## Installation
1. Installation de pip : # sudo apt-get install python-pip
2. Création d'un répertoire : # mkdir ~/django
3. Installation de virtualenv : # sudo pip install virtualenv
4. Création d'un venv avec python 3.3 : # virtualenv -p /usr/bin/python3.3 django/venv
5. Activation du virtualenv : # source django/venv/bin/activate
6. Installaion de Django : # pip install Django==1.5.4
7. Récupération du projet : # cd django && git clone https://bitbucket.org/klejoncour/django-biblio.git
8. Création de la base de données : # cd django-biblio && ./rebuild.sh
9. Lancement du projet : # python manage.py runserver