from django.forms import ModelForm,  Textarea
from django import forms
from biblio.models import *


class AuthorForm(ModelForm):
    def __init__(self, *args, **kwargs):
            super(AuthorForm, self).__init__(*args, **kwargs)
            self.fields['lastname'].label = "Nom "
            self.fields['firstname'].label = "Prenom "
            
    class Meta:
        model = Author
        fields =  ('lastname', 'firstname')

class BookForm(ModelForm):
    def __init__(self, *args, **kwargs):
            super(BookForm, self).__init__(*args, **kwargs)
            self.fields['title'].label = "Titre "
            self.fields['quantity'].label = "Quantité "
            self.fields['authors'].label = "Auteurs "
            self.fields['subject'].label = "Sujet "
            
    class Meta:
        model = Book
        fields =  ('title', 'quantity', 'authors', 'subject')