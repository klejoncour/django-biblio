from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Author(models.Model):
	lastname=models.CharField(max_length=50)
	firstname=models.CharField(max_length=50)
	
	def __str__(self):
		return "%s %s" %(self.firstname,self.lastname)


class Book(models.Model):
	title=models.CharField(max_length=255)
	quantity=models.IntegerField()
	users = models.ManyToManyField(User)
	authors = models.ManyToManyField("Author", related_name="books")
	subject = models.ForeignKey("Subject")

	def _get_dispo(self):
		return (self.quantity - self.users.count())
	dispo = property(_get_dispo)
	
	def __str__(self):
		return "%s" %(self.title)


class Subject(models.Model):
	label = models.CharField(max_length=50)
	
	def __str__(self):
		return "%s" %(self.label)
