# Create your views here.
from biblio.models import *
from biblio.forms import *
#from django.shortcuts import render_to_response
from tutoriel.shortcuts import render
from django.core.context_processors import csrf
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User, Group

def show_main(request):
	return render(
		request,
		"biblio/main.html",
		{})

################### BOOKS ###################
#@permission_required("biblio.can_edit_database")
def user_books(request):
  if request.user.has_perm('biblio.can_edit_database') or request.user.has_perm('biblio.can_take_book'):
    return render(
      request,
      "biblio/user_books.html",
      {"books": Book.objects.order_by("title")})
  else:
    return HttpResponseRedirect("/login?next=/user-books/")

def show_books(request):
	return render(
		request,
		"biblio/books.html",
		{"books": Book.objects.order_by("title")})

@permission_required("biblio.can_edit_database")
def add_book(request):
    bookform = BookForm()
    con ={'bookform': bookform}
    con.update(csrf(request))
    if len(request.POST) > 0:
        bookform =BookForm(request.POST)
        con = {'bookform': bookform}
        if bookform.is_valid():  
            task=bookform.save(commit=False)
            con.update(csrf(request))
            task.save()
            bookform.save_m2m()
            if request.GET.get('next'):
                return HttpResponseRedirect(request.GET.get('next'))
            else:
                return HttpResponseRedirect("/books/")
        else:
            return render(request,'biblio/add_book.html', con)     
        
    else:      
        return render(request,'biblio/add_book.html', con)

@permission_required("biblio.can_edit_database")
def edit_book(request, pk):
	instance = Book.objects.get(pk=pk)
	bookform = BookForm(instance=instance)
	con ={'bookform': bookform}
	con.update(csrf(request))
	if len(request.POST) > 0:
		bookform =BookForm(request.POST,instance=instance)
		con = {'bookform': bookform}
		if bookform.is_valid():   
			task=bookform.save(commit=False)
			con.update(csrf(request))
			task.save()
			bookform.save_m2m()
			if request.GET.get('next'):
				return HttpResponseRedirect(request.GET.get('next'))
			else:
				return HttpResponseRedirect("/books/")
		else:
			return render(request,'biblio/edit_book.html', con)     
        
	else:      
		return render(request,'biblio/edit_book.html', con)      

@permission_required("biblio.can_edit_database")
def delete_book(request, pk):
    instance = Book.objects.get(pk=pk)
    instance.delete()
    if request.GET.get('next'):
        return HttpResponseRedirect(request.GET.get('next'))
    else:
        return HttpResponseRedirect("/books/")

@permission_required("biblio.can_take_book")
def take_book(request, pk):
    instance = Book.objects.get(pk=pk)
    instance.users.add(request.user)
    instance.save()
    if request.GET.get('next'):
        return HttpResponseRedirect(request.GET.get('next'))
    else:
        return HttpResponseRedirect("/books/")

@permission_required("biblio.can_take_book")
def give_book(request, pk):
    instance = Book.objects.get(pk=pk)
    instance.users.remove(request.user)
    instance.save()
    if request.GET.get('next'):
        return HttpResponseRedirect(request.GET.get('next'))
    else:
        return HttpResponseRedirect("/books/")

################### AUTHOR ###################

def show_author(request, pk):
	return render(
		request,
		"biblio/author.html",
		{"author": Author.objects.get(pk=pk)})

def show_authors(request):
	return render(
		request,
		"biblio/authors.html",
		{"authors": Author.objects.order_by("lastname")})

@permission_required("biblio.can_edit_database")
def add_author(request):
    authorform = AuthorForm()
    con ={'authorform': authorform}
    con.update(csrf(request))
    if len(request.POST) > 0:
        authorform =AuthorForm(request.POST)
        con = {'authorform': authorform}
        if authorform.is_valid():   
            task=authorform.save(commit=False)
            con.update(csrf(request))
            task.save()
            if request.GET.get('next'):
                return HttpResponseRedirect(request.GET.get('next'))
            else:
                return HttpResponseRedirect("/authors/")
        else:
            return render(request,'biblio/add_author.html', con)     
        
    else:      
        return render(request,'biblio/add_author.html', con)  

@permission_required("biblio.can_edit_database")
def edit_author(request, pk):
	instance = Author.objects.get(pk=pk)
	authorform = AuthorForm(instance=instance)
	con ={'authorform': authorform}
	con.update(csrf(request))
	if len(request.POST) > 0:
		authorform =AuthorForm(request.POST,instance=instance)
		con = {'authorform': authorform}
		if authorform.is_valid():   
			task=authorform.save(commit=False)
			con.update(csrf(request))
			task.save()
			if request.GET.get('next'):
				return HttpResponseRedirect(request.GET.get('next'))
			else:
				return HttpResponseRedirect("/authors/")
		else:
			return render(request,'biblio/edit_author.html', con)     
        
	else:      
		return render('biblio/edit_author.html', con)      

def get_author_json(request, pk):
	author = Author.objects.get(pk=pk)
	data = {"name": author.lastname, "fname": author.firstname}
	
	import json
	text = json.dumps(data, indent=2, ensure_ascii=False)
	response = HttpResponse(text,mimetype='application/json; charset=utf8')
	return response

@permission_required("biblio.can_edit_database")
def delete_author(request, pk):
    instance = Author.objects.get(pk=pk)
    if instance.books.count() == 0:
        instance.delete()
        
    if request.GET.get('next'):
        return HttpResponseRedirect(request.GET.get('next'))
    else:
        return HttpResponseRedirect("/authors/")

################### USER ###################

def registration_page(request):
  state = ""
  username = password = password_confirm = ''
  
  if request.POST:
    username = request.POST.get('username')
    password = request.POST.get('password')
    password_confirm = request.POST.get('password_confirm')

    if User.objects.filter(username=username).exists():
      state = "Le nom d'utilisateur n'est pas disponible."
    else:
      if password == password_confirm:
        user = User.objects.create_user(username=username,password=password)
        user.groups.add(Group.objects.get(name="User"))
        user.save()

        user = authenticate(username=username, password=password)
        if user is not None:
          if user.is_active:
            login(request, user)
            if request.GET.get('next'):
              return HttpResponseRedirect(request.GET.get('next'))
            else:
              return HttpResponseRedirect('/')
          else:
            state = "Votre compte est désactivé, veuillez contacter votre administrateur."
        else:
          state = "La combinaison utilisateur / mot de passe est introuvable."
      else:
        state = "Le mot de passe et sa confirmation ne correspondent pas."

  ctx = {'state':state, 'username': username}
  ctx.update(csrf(request))
  return render(request,'biblio/registration.html',ctx)

def login_user(request):
   state = ""
   username = password = ''
   if request.POST:
       username = request.POST.get('username')
       password = request.POST.get('password')

       user = authenticate(username=username, password=password)
       if user is not None:
           if user.is_active:
               login(request, user)
               if request.GET.get('next'):
                   return HttpResponseRedirect(request.GET.get('next'))
               else:
                   return HttpResponseRedirect('/')
           else:
               state = "Votre compte est désactivé, veuillez contacter votre administrateur."
       else:
           state = "La combinaison utilisateur / mot de passe est introuvable."
   ctx = {'state':state, 'username': username}
   ctx.update(csrf(request))
   return render(request,'biblio/auth.html',ctx)


def logout_page(request):
   logout(request)
   return HttpResponseRedirect('/')
 
