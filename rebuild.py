from django.core.management import setup_environ
from tutoriel import settings
setup_environ(settings)

from biblio.models import Author, Book, Subject
import json

##### AUTEURS #####
authors = json.load(open("data/authors.json",encoding="utf-8"))
authorsTab = {}
for a in authors:
	author = Author(lastname=a["lastname"], firstname=a["firstname"])
	author.save()
	authorsTab[str(author)] = author

##### LIVRES #####
books = json.load(open("data/books.json",encoding="utf-8"))
subjectTab = {}
for b in books:
	s = b["subject"]
	if s not in subjectTab:
		subject = Subject(label=s)
		subject.save()
		subjectTab[s] = subject
		
	book = Book(title=b["title"], quantity=b["quantity"], subject=subjectTab[b["subject"]])
	book.save()
	for a in b["authors"]:
		book.authors.add(authorsTab[a])
	book.save()

##### PERMISSIONS #####
from django.contrib.auth.models import Permission, User, Group
from django.contrib.contenttypes.models import ContentType

author_content_type = ContentType.objects.get_for_model(Author)

perms = json.load(open("data/perms.json",encoding="utf-8"))
permsTab = {}
for p in perms:
	perm = Permission.objects.create(codename=p["codename"], name=p["name"], content_type=author_content_type)
	perm.save()
	permsTab[p["codename"]] = perm
	
##### GROUPES #####
groups = json.load(open("data/groups.json",encoding="utf-8"))
groupsTab = {}
for g in groups:
	group = Group.objects.create(name=g["name"])
	group.save()
	for p in g["permissions"]:
		group.permissions.add(permsTab[p])
	group.save()
	groupsTab[g["name"]] = group
	
##### UTILISATEURS #####
users = json.load(open("data/users.json",encoding="utf-8"))
usersTab = {}
for u in users:
	user = User.objects.create_user(username=u["username"],password=u["password"])
	user.save()
	for g in u["groups"]:
		user.groups.add(groupsTab[g])
	user.save()
	usersTab[u["username"]] = user
