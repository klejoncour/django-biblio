from tastypie.resources import ModelResource
from biblio.models import Author, Book, Subject

class AuthorResource(ModelResource):
	class Meta:
		queryset = Author.objects.all()
		resources_name = "author"
