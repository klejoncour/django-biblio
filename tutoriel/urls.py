from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

#from biblio.api import AuthorResource
#author_resource = AuthorResource()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tutoriel.views.home', name='home'),
    # url(r'^tutoriel/', include('tutoriel.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^/?$', "biblio.views.show_main"),
    
    url(r'^author/(\d+)$', "biblio.views.show_author"),
    url(r'^authors/?$', "biblio.views.show_authors"),
    url(r'^authors/add_author.html$', "biblio.views.add_author"),
    url(r'^authors/delete/(\d+)$', "biblio.views.delete_author"),
    url(r'^authors/edit/(\d+)$', "biblio.views.edit_author"),
    
    url(r'^books/?$', "biblio.views.show_books"),
    url(r'^books/add_book.html$', "biblio.views.add_book"),
    url(r'^books/delete/(\d+)$', "biblio.views.delete_book"),
    url(r'^books/edit/(\d+)$', "biblio.views.edit_book"),
    url(r'^books/take/(\d+)$', "biblio.views.take_book"),
    url(r'^books/give/(\d+)$', "biblio.views.give_book"),

    url(r'^user-books/?$', "biblio.views.user_books"),
    
    url(r'^registration/$', "biblio.views.registration_page"),
    url(r'^login/$', "biblio.views.login_user"),
    url(r'^logout/$', "biblio.views.logout_page"),
    url(r'^get-author-json/(\d+)$', "biblio.views.get_author_json"),
    #url(r'^api/', include(author_resources.urls)),
)
